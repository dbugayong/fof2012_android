package com.mci.fof.util;

public class Constants {
	public static final String TOKEN = "9d90aa0ac2d4c38dfd621aa2fdd22362";
	public static final String URL = "http://letsflyourflag.sg/fof_mock_ws/index.php";
	public static final String UPLOAD_URL = "http://letsflyourflag.sg/fof_mock_ws/upload_video.php";
	public static final String CACHED_IMGS_DIR = "/data/data/com.mci.fof/images/";
	public static final String APP_TITLE = "Fly Our Flag 2012";
	public static final String SHARED_PREFS = "FOF_USER";
}
