package com.mci.fof.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import com.mci.fof.adapter.EventAdapter;
import com.mci.fof.adapter.VideoAdapter;
import com.mci.fof.dto.EventDTO;
import com.mci.fof.dto.UserDTO;
import com.mci.fof.dto.VideoDTO;

public class DataUtils {
	
	Context context;
	public String rec_cnt = null;
	public String page_cnt = null;
	public static String current_nric = null;
	
	public DataUtils(Context context, String nric) {
		this.context = context;
		current_nric = nric;
	}
	
	public DataUtils(Context context) {
		this.context = context;
	}

	public String getRec_cnt() {
		return this.rec_cnt;
	}

	public void setRec_cnt(String rec_cnt) {
		this.rec_cnt = rec_cnt;
	}

	public String getPage_cnt() {
		return this.page_cnt;
	}

	public void setPage_cnt(String page_cnt) {
		this.page_cnt = page_cnt;
	}

	public EventAdapter getEvents(String page) throws JSONException {
		EventAdapter ea = new EventAdapter(context);
		Log.i("JSON URL", Constants.URL + "/fof/eventspaged/" + page + "/10");
        JSONUtil util = new JSONUtil(Constants.URL + "/fof/eventspaged/" + page + "/10");
        JSONObject message = util.getResponse();
        setPage_cnt(message.getString("page_cnt"));
        setRec_cnt(message.getString("count"));
        Log.i("DataUtils", "Count: " + message.getString("count") + "<br/>Pages: " + message.getString("page_cnt"));
		JSONArray events = message.getJSONArray("events");
		EventDTO dto = null;
		for(int i = 0; i < events.length(); i++) {
			JSONObject e = events.getJSONObject(i);
			dto = new EventDTO();
			dto.setEvent_id(e.getString("event_id"));
			dto.setCc_id(e.getString("cc_id"));
			dto.setName(e.getString("name"));
			dto.setEvent_date(e.getString("date"));
			dto.setStart_time(e.getString("start_time"));
			dto.setEvent_desc(e.getString("desc"));
			dto.setImage_url(e.getString("image_url"));
			dto.setAdmin_id(e.getString("admin_id"));
			dto.setFlag(e.getString("flag"));
			ea.addItem(dto);
		}
		return ea;
	}
	
	public VideoAdapter getVideos(String page) throws JSONException {
		VideoAdapter va = new VideoAdapter(context);
		Log.i("JSON URL [doUploadVideo]", Constants.URL + "/fof/videospaged/" + page + "/10");
        JSONUtil util = new JSONUtil(Constants.URL + "/fof/videospaged/" + page + "/10");
        JSONObject message = util.getResponse();
        setPage_cnt(message.getString("page_cnt"));
        setRec_cnt(message.getString("count"));
		JSONArray videos = message.getJSONArray("videos");
		VideoDTO dto = null;
		for(int i = 0; i < videos.length(); i++) {
			JSONObject vid = videos.getJSONObject(i);
			dto = new VideoDTO();
			dto.setVideo_id(vid.getString("video_id"));
			dto.setTitle(vid.getString("title"));
			dto.setDescription(vid.getString("description").trim());
			dto.setFile_path(vid.getString("file_path"));
			dto.setThumb(vid.getString("thumb"));
			dto.setFlag(vid.getString("flag"));
			va.addItem(dto);
		}
		return va;
	}
	
	public JSONObject doRegister(UserDTO dto) {
		int TIMEOUT_MILLISEC = 60000;  // = 60 seconds
		
		String url = Constants.URL + "/fof";
		Log.i("JSON URL [doUploadVideo]", url);
		HttpPost request = new HttpPost(url);
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
		HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
		HttpClient client = new DefaultHttpClient(httpParams);
		StringBuilder builder = new StringBuilder();
		JSONObject message = null;
		
		try {
			request.setHeader("Content-type", "application/x-www-form-urlencoded");
			request.setHeader("Authorization", Constants.TOKEN);
			
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(8);
	        nameValuePairs.add(new BasicNameValuePair("action", "reg"));
	        nameValuePairs.add(new BasicNameValuePair("salutation", dto.getSalutation()));
	        nameValuePairs.add(new BasicNameValuePair("fullname", dto.getFullname()));
	        nameValuePairs.add(new BasicNameValuePair("nric", dto.getNric()));
	        nameValuePairs.add(new BasicNameValuePair("mobile", dto.getMobile()));
	        nameValuePairs.add(new BasicNameValuePair("email", dto.getEmail()));
	        nameValuePairs.add(new BasicNameValuePair("password", dto.getPassword()));
	        nameValuePairs.add(new BasicNameValuePair("user_type", "1"));
	        
	        request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        
			HttpResponse response = client.execute(request);
			HttpEntity entity = response.getEntity();
			
			InputStream content = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
			
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			
			message = new JSONObject(builder.toString());
			int result = message.getInt("result");
			if(result == 1) {
				Log.i("doRegister", "Request result is " + result);
			}
		} 
		catch (ClientProtocolException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return message;
	}
	
	public JSONObject doLogin(String nric, String password) {
		int TIMEOUT_MILLISEC = 60000;  // = 60 seconds
		String url = Constants.URL + "/fof";
		Log.i("JSON URL [doUploadVideo]", url);
		HttpPost request = new HttpPost(url);
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
		HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
		HttpClient client = new DefaultHttpClient(httpParams);
		StringBuilder builder = new StringBuilder();
		JSONObject message = null;
		
		try {
			request.setHeader("Content-type", "application/x-www-form-urlencoded");
			request.setHeader("Authorization", Constants.TOKEN);
			
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(8);
	        nameValuePairs.add(new BasicNameValuePair("action", "login"));
	        nameValuePairs.add(new BasicNameValuePair("nric", nric));
	        nameValuePairs.add(new BasicNameValuePair("password", password));
	        
	        request.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	        
			HttpResponse response = client.execute(request);
			HttpEntity entity = response.getEntity();
			
			InputStream content = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
			
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			
			message = new JSONObject(builder.toString());
			int result = message.getInt("result");
			if(result == 1) {
				Log.i("doLogin", "Request result is " + result);
			}
		} 
		catch (ClientProtocolException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return message;
	}
	
	public JSONObject doUploadVideo(File aFile, String title, String desc, String nric) {
		Log.i("JSON URL [doUploadVideo]", Constants.UPLOAD_URL);
		String aUrl = Constants.UPLOAD_URL; 
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(aUrl);
        JSONObject message = null;
        try {
            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
            entity.addPart("user_file", new FileBody(aFile));
            entity.addPart("title", new StringBody(title));
            entity.addPart("description", new StringBody(desc));
            entity.addPart("nric", new StringBody(nric));
            httpPost.setEntity(entity);

            HttpContext localContext = new BasicHttpContext();
            HttpResponse response = httpClient.execute(httpPost, localContext);
            HttpEntity resEntity = response.getEntity();  
            String resp = "";
            if (response != null) {    
                resp = EntityUtils.toString(resEntity); 
            }
			message = new JSONObject(resp);
            Log.i("doUploadVideo RESULT", message.getString("result"));
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        catch (JSONException e) {
			e.printStackTrace();
		}
        return message;
    }
	
	public JSONObject doVote(String nric, String video_id) {
		Log.i("JSON URL [doVote]", Constants.URL + "/fof/vote/" + nric + "/" + video_id);
        JSONUtil util = new JSONUtil(Constants.URL + "/fof/vote/" + nric + "/" + video_id);
        JSONObject message = util.getResponse();
        return message;
	}
	
	public static String getNric() {
		return current_nric;
	}
	
	public static String getEmailMessage(String vid, String url) {
		Spanned m = Html.fromHtml(new StringBuilder()
				.append("Hello There!<br/><br/>")
				.append("In celebration of Singapore's 47th birthday, I'm taking part in \"My Gift to Singapore\" video contest organised by the People's Association.<br/><br/>")
				.append("I hope you'll take part too by submitting your own videos via the website, iPhone or Android App and stand a chance to win a prize! Visit www.letsflyourflag.sg for more information<br/><br/>")
				.append("And do remember to display the Singapore flag too!<br/><br/>")
				.append("Here's a link to a video I want to share with you!<br/><br/>")
				.append("http://letsflyourflag.sg/view_video.php?vid=" + vid + "<br/><br/>")
				.append("<u>About the contest</u><br/>")
				.append("Tell a short story based the theme of \"My Gift to Singapore\" at around 47 seconds. The video contest serves as a channel for ")
				.append("Singaporeans to show their love and patriotism towards the country. Through this contest, we hope to create an online community and space ")
				.append("for social interaction and sharing, and to encourage people out there to discover nuggets of interesting stories of all things Uniquely Singapore.")
				.append(" With the rise of social media, it will help to heighten the excitement of National Day Celebrations through the videos and allow all to feel our ")
				.append("heritage and let Singapore's heritage come alive with just a click of mouse.").toString());
		return m.toString();
	}

	String u = null;
	public Bitmap loadBitmap(String url) {
		this.u = url;
		Bitmap bm = null;
		try {
			Log.i("loadBitmap", url);
			bm = BitmapFactory.decodeStream(new FlushedInputStream((InputStream) new URL(url).getContent()));
		}
		catch (MalformedURLException e) {
			try {
				bm = BitmapFactory.decodeStream(new FlushedInputStream((InputStream) new URL("http://www.letsflyourflag.sg/uploaded_files/default_photo.jpg").getContent()));
			} 
			catch (MalformedURLException e1) {
				Log.i("DataUtils", "MalformedURLException: " + this.u);
			} 
			catch (IOException e1) {
				Log.i("DataUtils", "IOException");
			}
		}
		catch (IOException e) {
			try {
				bm = BitmapFactory.decodeStream(new FlushedInputStream((InputStream) new URL("http://www.letsflyourflag.sg/uploaded_files/default_photo.jpg").getContent()));
			} 
			catch (MalformedURLException e1) {
				Log.i("DataUtils", "MalformedURLException: " + this.u);
			} 
			catch (IOException e1) {
				Log.i("DataUtils", "IOException");
			}
		}
		return bm;
	}
	
}
