package com.mci.fof.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;


public class JSONUtil {

	private String url = null;

	public JSONUtil(String URL) {
		this.url = URL;
	}
	
	public JSONObject getResponse() {

		JSONObject message = new JSONObject();
		HttpGet request = new HttpGet(url);
		HttpParams httpParams = new BasicHttpParams();
		HttpClient client = new DefaultHttpClient(httpParams);
		
		StringBuilder builder = new StringBuilder();
		try {

			request.setHeader("Accept", "application/json");
			request.setHeader("Content-type", "application/json");
			request.setHeader("Authorization", Constants.TOKEN);
			
			HttpResponse response = client.execute(request);
			HttpEntity entity = response.getEntity();
			
			InputStream content = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
			
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			
			//Raw JSON response 
			Log.i("DENNIS", "response = "+ builder.toString());
			message = new JSONObject(builder.toString());
			int result = message.getInt("result");
			if(result == 1){ 
				Log.i("FOF2012", "FOF JSON request success!");
			}
		} 
		catch (ClientProtocolException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (JSONException e) {
			e.printStackTrace();
		}
		return message;
	}

}
