package com.mci.fof.tv;

import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

public class EventTextView extends LinearLayout {
	
	public TextView event_id = null;
	public TextView name = null;
	public TextView event_desc = null;
	public TextView event_date = null;
	public TextView end_date = null;
	public TextView start_time = null;
	public TextView end_time = null;
	public TextView cc_name = null;
	public TextView image_url = null;
	public TextView venue = null;
	
	public EventTextView(Context context, String event_id, String name, String event_desc, String event_date, String end_date, String start_time, String thumb, String end_time, String cc_name, String image_url, String venue) {
		super(context);
		this.setGravity(Gravity.CENTER_VERTICAL);
		this.setOrientation(HORIZONTAL);
		this.setPadding(10, 5, 0, 5);
		this.setBackgroundColor(android.R.color.white);
		
//		DataUtils utils = new DataUtils(context);
//		ImageView tn = new ImageView(context);
//		Bitmap bm = utils.loadBitmap(thumb);
//		Bitmap resizedbitmap = Bitmap.createScaledBitmap(bm, 150, 120, true);
//		tn.setImageBitmap(resizedbitmap);
//		addView(tn);
		
		LinearLayout linearLayout = new LinearLayout(context);
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		linearLayout.setPadding(10, 10, 10, 10);
		addView(linearLayout);
		
		this.event_id = new TextView(context, null, android.R.attr.textAppearanceSmall);
		this.event_id.setText(event_id);
		
		this.end_time = new TextView(context, null, android.R.attr.textAppearanceSmall);
		this.end_time.setText(end_time);

		this.end_date = new TextView(context, null, android.R.attr.textAppearanceSmall);
		this.end_date.setText(end_date);

		this.cc_name = new TextView(context, null, android.R.attr.textAppearanceSmall);
		this.cc_name.setText(cc_name);

		this.image_url = new TextView(context, null, android.R.attr.textAppearanceSmall);
		this.image_url.setText(image_url);

		this.venue = new TextView(context, null, android.R.attr.textAppearanceSmall);
		this.venue.setText(venue);

		this.name = new TextView(context, null, android.R.attr.textAppearanceSmall);
		this.name.setText(name);
		this.name.setTextColor(getResources().getColor(android.R.color.black));
		this.name.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
		this.name.setSingleLine(true);
		
		this.event_desc = new TextView(context, null, android.R.attr.textAppearanceSmall);
		this.event_desc.setText(event_desc.trim());
		this.event_desc.setTextColor(getResources().getColor(android.R.color.black));
		this.event_desc.setPadding(0, 0, 0, 10);
		this.event_desc.setSingleLine(true);

		this.event_date = new TextView(context, null, android.R.attr.textAppearanceSmall);
		this.event_date.setText(event_date);
		this.event_date.setTextColor(getResources().getColor(android.R.color.black));
		this.event_date.setPadding(0, 0, 0, 10);
		this.event_date.setSingleLine(true);

		this.start_time = new TextView(context, null, android.R.attr.textAppearanceSmall);
		this.start_time.setText(start_time);
		this.start_time.setTextColor(getResources().getColor(android.R.color.black));
		this.start_time.setPadding(0, 0, 0, 10);
		this.start_time.setSingleLine(true);

		linearLayout.addView(this.name, new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		linearLayout.addView(this.event_desc, new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		linearLayout.addView(this.event_date, new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		linearLayout.addView(this.start_time, new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
	}

	public TextView getEvent_id() {
		return event_id;
	}

	public void setEvent_id(String event_id) {
		this.event_id.setText(event_id);
	}

	public TextView getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.event_id.setText(venue);
	}

	public TextView getName() {
		return name;
	}

	public void setName(String name) {
		this.name.setText(name);
	}

	public TextView getEvent_desc() {
		return event_desc;
	}

	public void setEvent_desc(String event_desc) {
		this.event_desc.setText(event_desc);
	}

	public TextView getEvent_date() {
		return event_date;
	}

	public void setEvent_date(String event_date) {
		this.event_date.setText(event_date);
	}

	public TextView getEnd_date() {
		return event_date;
	}

	public void setEnd_date(String end_date) {
		this.event_date.setText(end_date);
	}

	public TextView getStart_time() {
		return start_time;
	}

	public void setStart_time(String start_time) {
		this.start_time.setText(start_time);
	}

	public TextView getEnd_time() {
		return end_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time.setText(end_time);
	}

	public TextView getCc_name() {
		return cc_name;
	}

	public void setCc_name(String cc_name) {
		this.cc_name.setText(cc_name);
	}

	public TextView getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url.setText(image_url);
	}

	public void setEvent_id(TextView event_id) {
		this.event_id = event_id;
	}

}
