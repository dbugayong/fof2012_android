package com.mci.fof.tv;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mci.fof.activity.FakeActivity;
import com.mci.fof.activity.R;
import com.mci.fof.util.Constants;
import com.mci.fof.util.DataUtils;

public class VideoTextView extends LinearLayout {
	
	public TextView event_id = null;
	public TextView title = null;
	public TextView description = null;
	public TextView file_path = null;
	final String items[] = {"Facebook", "Twitter", "Email" };

	public VideoTextView (Context context, String video_id, String title, String description, String file_path, String thumb) {
		super(context);
		this.setGravity(Gravity.CENTER_VERTICAL);
		this.setOrientation(HORIZONTAL);
		this.setPadding(10, 10, 0, 12);
		this.setBackgroundColor(android.R.color.white);
		final Context con = context;

		DataUtils utils = new DataUtils(context);
		ImageView tn = new ImageView(context);
		Bitmap bm = utils.loadBitmap(thumb);
		Bitmap resizedbitmap = Bitmap.createScaledBitmap(bm, 150, 130, true);
		tn.setImageBitmap(resizedbitmap);
		
		addView(tn);
				
		LinearLayout linearLayout = new LinearLayout(context);
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		linearLayout.setPadding(10, 10, 10, 10);
		addView(linearLayout);
		
		this.title = new TextView(context, null, android.R.attr.textAppearanceSmall);
		this.title.setText(title);
		this.title.setTextColor(getResources().getColor(android.R.color.black));
		this.title.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
		this.title.setSingleLine(true);

		this.description = new TextView(context, null, android.R.attr.textAppearanceSmall);
		this.description.setText(description);
		this.description.setTextColor(getResources().getColor(android.R.color.darker_gray));
		this.description.setPadding(0, 0, 0, 10);
		this.description.setSingleLine(true);

		LinearLayout buttonLayout = new LinearLayout(context);
		buttonLayout.setOrientation(LinearLayout.HORIZONTAL);
		
		final String vid = video_id;
		ImageView shareButton = new ImageView(context);
		shareButton.setImageResource(R.drawable.share);
		shareButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				AlertDialog.Builder ab = new AlertDialog.Builder(con);
				ab.setTitle(Constants.APP_TITLE);
				ab.setItems(items, new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						if(which == 0) {
						    Uri uri = Uri.parse("http://m.facebook.com/sharer.php?u=http://letsflyourflag.sg/view_video.php?vid=" + vid);
						    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
						    con.startActivity(intent);
						}
						else if(which == 1) {
						    Uri uri = Uri.parse("http://twitter.com/home?status=Check out this video at Fly Our Flag 2012 http://letsflyourflag.sg/view_video.php?vid=" + vid);
						    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
						    con.startActivity(intent);
						}
						else if(which == 2) {
							Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
						    emailIntent.setType("plain/text");
						    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Let's Fly Our Flag 2012 Video Contest. Join now!");
						    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, Html.fromHtml(new StringBuilder()
							.append("Hello There!<br/><br/>")
							.append("Check this <a href=\"http://letsflyourflag.sg/view_video.php?vid=" + vid + "\">video</a> I want to share with you!<br/><br/>")
							.append("In celebration of Singapore's 47th birthday, I&#39;m taking part in &quot;My Gift to Singapore&quot; video contest organised by the People&#39;s Association<br/><br/>")
							.append("I hope you&#39;ll take part too by submitting your own videos via the website, iPhone or Android App and stand a chance to win a prize! Visit <a href=\"http://www.letsflyourflag.sg\">www.letsflyourflag.sg</a> for more information. And do remember to display the Singapore flag too! <br/><br/> ")
							.append("<u>About the contest</u><br/><br/>")
							.append("Tell a short story based the theme of \"My Gift to Singapore\" at around 47 seconds. The video contest serves as a channel for ")
							.append("Singaporeans to show their love and patriotism towards the country. Through this contest, we hope to create an online community and space ")
							.append("for social interaction and sharing, and to encourage people out there to discover nuggets of interesting stories of all things Uniquely Singapore.")
							.append(" With the rise of social media, it will help to heighten the excitement of National Day Celebrations through the videos and allow all to feel our ")
							.append("heritage and let Singapore's heritage come alive with just a click of mouse.").toString()));
						    con.startActivity(emailIntent);
						}
					}
				});
				ab.show();
			}
		});
		
		ImageView voteButton = new ImageView(context);
		voteButton.setImageResource(R.drawable.love);
		voteButton.setPadding(15, 0, 0, 0);
		voteButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
	        	if(!DataUtils.getNric().equals("123456789")) {
					DataUtils utils = new DataUtils(con, DataUtils.getNric());
					final JSONObject message = utils.doVote(DataUtils.getNric(), vid);
					try {
						Log.i("SUCCESS", message.getString("msg"));
		        		AlertDialog.Builder aa = new AlertDialog.Builder(con);
		        		aa.setTitle(Constants.APP_TITLE);
		        		aa.setMessage(message.getString("msg") + "!");
		        		aa.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// do nothing
							}
						});
		        		aa.show();
					} 
					catch (JSONException e) {
						e.printStackTrace();
					}
	        	}
	        	else {
					Intent intent = new Intent(con, FakeActivity.class);
					intent.putExtra("vid", vid);
					con.startActivity(intent);
	        	}
			}
		});
		
		buttonLayout.addView(shareButton, new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		buttonLayout.addView(voteButton, new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		
		linearLayout.addView(this.title, new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		linearLayout.addView(this.description, new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
		linearLayout.addView(buttonLayout);
		
		this.file_path = new TextView(context, null, android.R.attr.textAppearanceSmall);
		this.file_path.setText(file_path);
	}

	public TextView getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path.setText(file_path);
	}

	public TextView getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title.setText(title);
	}

	public TextView getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description.setText(description);
	}

}
