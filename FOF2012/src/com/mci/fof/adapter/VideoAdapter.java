package com.mci.fof.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mci.fof.dto.VideoDTO;
import com.mci.fof.tv.VideoTextView;

public class VideoAdapter extends BaseAdapter {
	
	private ArrayList<VideoDTO> video;
	private Context context;
	
	public VideoAdapter(Context con) {
		context = con;
		video = new ArrayList<VideoDTO>();
	}
	
	public void addVideo(VideoDTO dto) {
		video.add(dto);
	}
	
    public void setListItems(ArrayList<VideoDTO> list) {
        video = list;
    }
    
    public int getCount() {
    	return video.size();
    }

    public Object getVideo(int position) {
        return video.get(position);
    }

    public boolean areAllItemsSelectable() {
        return false;
    }
 
    public boolean isSelectable(int position) {
        return video.get(position).isSelectable();
    }
 
    public long getVideoID(int position) {
        return position;
    }

    public View getView(int position, View convetview, ViewGroup parent) {
		VideoTextView tv;
	    if (convetview == null) {
			tv = new VideoTextView(context, video.get(position).getVideo_id(), video.get(position).getTitle(), video.get(position).getDescription(), video.get(position).getFile_path(), video.get(position).getThumb());
	    } 
	    else {
	        tv = (VideoTextView) convetview;
//	        tv.setVideo_id(video.get(position).getVideo_id());
	        tv.setTitle(video.get(position).getTitle());
	        tv.setDescription(video.get(position).getDescription());
//	        tv.setNric(video.get(position).getNric());
	        tv.setFile_path(video.get(position).getFile_path());
//	        tv.setThumb(video.get(position).getThumb());
//	        tv.setFlag(video.get(position).getFlag());
	    }
	    return tv;
    }

    public void addItem(VideoDTO dto) {
        video.add(dto);	        
    }
    
    public void removeItem(int position) {
    	video.remove(position);
    }
    
    public Object getItem(int position) {
        return video.get(position);
    }
 
    public long getItemId(int position) {
        return position;
    }
}
