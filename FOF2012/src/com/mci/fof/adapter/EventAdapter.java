package com.mci.fof.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mci.fof.dto.EventDTO;
import com.mci.fof.tv.EventTextView;

public class EventAdapter extends BaseAdapter {

	private ArrayList<EventDTO> event;
	private Context context;
	
	public EventAdapter(Context con) {
		context = con;
		event = new ArrayList<EventDTO>();
	}
	
	public void addEvent(EventDTO dto) {
		event.add(dto);
	}
	
    public void setListItems(ArrayList<EventDTO> list) {
        event = list;
    }
    
    public int getCount() {
    	return event.size();
    }

    public Object getVideo(int position) {
        return event.get(position);
    }

    public boolean areAllItemsSelectable() {
        return false;
    }
 
    public boolean isSelectable(int position) {
        return event.get(position).isSelectable();
    }
 
    public long getVideoID(int position) {
        return position;
    }

    public View getView(int position, View convetview, ViewGroup parent) {
		EventTextView tv;
	    if (convetview == null) {
			tv = new EventTextView(context, event.get(position).getEvent_id(), event.get(position).getName(), event.get(position).getEvent_desc(), event.get(position).getEvent_date(), event.get(position).getEnd_date(), event.get(position).getStart_time(), event.get(position).getImage_url(), event.get(position).getEnd_time(), event.get(position).getCc_name(), event.get(position).getImage_url(), event.get(position).getVenue());
	    } 
	    else {
	        tv = (EventTextView) convetview;
	        tv.setEvent_id(event.get(position).getEvent_id());
	        tv.setName(event.get(position).getName());
	        tv.setEvent_desc(event.get(position).getEvent_desc());
	        tv.setEvent_date(event.get(position).getEvent_date());
	        tv.setStart_time(event.get(position).getStart_time());
	        tv.setEnd_time(event.get(position).getEnd_time());
	        tv.setCc_name(event.get(position).getCc_name());
	        tv.setImage_url(event.get(position).getImage_url());
	    }
	    return tv;
    }

    public void addItem(EventDTO dto) {
        event.add(dto);	        
    }
    
    public void removeItem(int position) {
    	event.remove(position);
    }
    
    public Object getItem(int position) {
        return event.get(position);
    }
 
    public long getItemId(int position) {
        return position;
    }

}
