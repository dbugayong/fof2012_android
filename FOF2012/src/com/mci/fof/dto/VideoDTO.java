package com.mci.fof.dto;


public class VideoDTO {
	
	public String video_id = null;
	public String title = null;
	public String description = null;
	public String nric = null;
	public String file_path = null;
	public String thumb = null;
	public String flag = null;
	public String t_stamp = null;
	public boolean isSelectable;
	
	public String getVideo_id() {
		return video_id;
	}
	
	public void setVideo_id(String video_id) {
		this.video_id = video_id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getNric() {
		return nric;
	}
	
	public void setNric(String nric) {
		this.nric = nric;
	}
	
	public String getFile_path() {
		return file_path;
	}
	
	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}
	
	public String getThumb() {
		return thumb;
	}
	
	public void setThumb(String thumb) {
		this.thumb = thumb;
	}
	
	public String getFlag() {
		return flag;
	}
	
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	public String getT_stamp() {
		return t_stamp;
	}
	
	public void setT_stamp(String t_stamp) {
		this.t_stamp = t_stamp;
	}
	
	public boolean isSelectable() {
		return isSelectable;
	}
	
	public void setSelectable(boolean isSelectable) {
		this.isSelectable = isSelectable;
	}
}
