package com.mci.fof.dto;

public class EventDTO {
	
	public String event_id = null;
	public String cc_id = null;
	public String cc_name = null;
	public String name = null;
	public String event_date = null;
	public String end_date = null;
	public String start_time = null;
	public String end_time = null;
	public String venue = null;
	public String event_desc = null;
	public String image_url = null;
	public String admin_id = null;
	public String flag = null;
	public boolean isSelectable;

	public String getCc_name() {
		return cc_name;
	}

	public void setCc_name(String cc_name) {
		this.cc_name = cc_name;
	}

	public String getEvent_id() {
		return event_id;
	}
	
	public void setEvent_id(String event_id) {
		this.event_id = event_id;
	}
	
	public String getCc_id() {
		return cc_id;
	}
	
	public void setCc_id(String cc_id) {
		this.cc_id = cc_id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEvent_date() {
		return event_date;
	}
	
	public void setEvent_date(String event_date) {
		this.event_date = event_date;
	}
	
	public String getEnd_date() {
		return end_date;
	}
	
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	
	public String getStart_time() {
		return start_time;
	}
	
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
	
	public String getEnd_time() {
		return end_time;
	}
	
	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}
	
	public String getVenue() {
		return venue;
	}
	
	public void setVenue(String venue) {
		this.venue = venue;
	}
	
	public String getEvent_desc() {
		return event_desc;
	}
	
	public void setEvent_desc(String event_desc) {
		this.event_desc = event_desc;
	}
	
	public String getImage_url() {
		return image_url;
	}
	
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	
	public String getAdmin_id() {
		return admin_id;
	}
	
	public void setAdmin_id(String admin_id) {
		this.admin_id = admin_id;
	}
	
	public String getFlag() {
		return flag;
	}
	
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	public boolean isSelectable() {
		return isSelectable;
	}
	
	public void setSelectable(boolean isSelectable) {
		this.isSelectable = isSelectable;
	}

}
