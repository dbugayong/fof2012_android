package com.mci.fof.activity;

import android.app.Activity;
import android.os.Bundle;

public class AboutPagesActivity extends Activity {
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle b = this.getIntent().getExtras();
        String about_index = b.getString("about_index");
        
        if(about_index.equals("flag")) {
        	setContentView(R.layout.about_flag);
        }
        else if(about_index.equals("contest")) {
        	setContentView(R.layout.about_contest);
		}
        else if(about_index.equals("tc")) {
        	setContentView(R.layout.about_tc);
        }
        else if(about_index.equals("faq")) {
        	setContentView(R.layout.about_faq);
        }
        else if(about_index.equals("pa")) {
        	setContentView(R.layout.about_pa);
        }
        else if(about_index.equals("oc")) {
        	setContentView(R.layout.about_oc);
        }
        
//        TextView tv = new TextView(this);
//        tv.setText(about_index);
//        setContentView(tv);
        
//        setContentView(R.layout.about);
        
//        ImageButton btn_home = (ImageButton) findViewById(R.id.btn_home);
//        btn_home.setOnClickListener(new View.OnClickListener() {
//			public void onClick(View v) {
//	            AboutActivity.this.finish();
//			}
//		});
    }


}
