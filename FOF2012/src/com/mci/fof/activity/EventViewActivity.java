package com.mci.fof.activity;

import com.mci.fof.util.DataUtils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class EventViewActivity extends Activity {
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_view);
        Bundle b = getIntent().getExtras();
        String title = b.getString("name");
        String thumb = b.getString("thumb");
        String where = b.getString("where");
        String when = b.getString("when");
        String timing = b.getString("time");
        String description = b.getString("desc");
        
        DataUtils utils = new DataUtils(this);
        ImageView _thumb = (ImageView) findViewById(R.id.thumb);
		Bitmap bm = utils.loadBitmap(thumb);
		_thumb.setImageBitmap(bm);
        
        TextView tv_title = (TextView) findViewById(R.id.name);
        tv_title.setText(title);
        
        TextView tv_where = (TextView) findViewById(R.id.venue);
        tv_where.setText(where);
        
        TextView tv_when = (TextView) findViewById(R.id.event_date);
        tv_when.setText(when);
        
        TextView tv_time = (TextView) findViewById(R.id.event_time);
        tv_time.setText(timing);
        
        TextView tv_desc_lbl = (TextView) findViewById(R.id.event_desc_lbl);
        TextView tv_desc = (TextView) findViewById(R.id.event_desc);
        tv_desc.setText(description);
        
        if(description == null || description == "") {
        	tv_desc.setVisibility(View.GONE);
        	tv_desc_lbl.setVisibility(View.GONE);
        }
        
        Button done = (Button) findViewById(R.id.btn_done);
        done.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				EventViewActivity.this.finish();
			}
		});
    }

}
