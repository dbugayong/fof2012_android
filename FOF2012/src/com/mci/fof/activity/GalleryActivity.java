package com.mci.fof.activity;

import org.json.JSONException;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.mci.fof.adapter.VideoAdapter;
import com.mci.fof.tv.VideoTextView;
import com.mci.fof.util.Constants;
import com.mci.fof.util.DataUtils;

public class GalleryActivity extends ListActivity {
	
	private static int p = 1;
	private static int pc = 0;
	DataUtils utils;
	VideoAdapter va;

	private ProgressDialog mDialog;
	protected InitTask initTask;
	
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	super.handleMessage(msg);
        	switch (msg.what) {
			case 10:
				setListAdapter(va);
				mDialog.dismiss();
				break;
			case 1:
				mDialog.setMessage("Fetching videos...");
	        	mDialog.setCancelable(false);
				mDialog.show();
				break;
			default:
				break;
			}
        }
    };
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.white);

        setContentView(R.layout.gallery);
        SharedPreferences prefs = getSharedPreferences(Constants.SHARED_PREFS, MODE_PRIVATE);
        utils = new DataUtils(GalleryActivity.this, prefs.getString("nric", "123456789"));
        
        ImageButton btn_home = (ImageButton) findViewById(R.id.btn_home);
        btn_home.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
	            GalleryActivity.this.finish();
			}
		});
        
		mDialog = new ProgressDialog(GalleryActivity.this);
		
        initTask = new InitTask();
        initTask.execute(GalleryActivity.this);
        setListAdapter(va);
        ListView lv = getListView();
        lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				VideoTextView tv = (VideoTextView) arg1;
				String video_url = tv.getFile_path().getText().toString();
				Intent intent = new Intent(Intent.ACTION_VIEW); intent.setDataAndType(Uri.parse(video_url), "video/mp4"); startActivity(intent);				
			}
        });
        
        ImageButton btn_next = (ImageButton) findViewById(R.id.btn_right);
        btn_next.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(p == pc) {
					Toast.makeText(getApplicationContext(), "This is the last page", Toast.LENGTH_LONG).show();
				}
				else {
					p = p + 1;
					mDialog = new ProgressDialog(GalleryActivity.this);
					mDialog.setMessage("Fetching next set of data for \r\nPage " + p);
		        	mDialog.setCancelable(false);
		        	mDialog.show();
		        	new Thread(new Runnable() {
		        		public void run() {
					        try {
								va = utils.getVideos(String.valueOf(p));
								va.notifyDataSetChanged();
							} 
					        catch (JSONException e) {
								e.printStackTrace();
							}
				            handler.sendEmptyMessage(10);
			    		}
			    	}).start();
				}
			}
		});
        
        ImageButton btn_refresh = (ImageButton) findViewById(R.id.btn_refresh);
        btn_refresh.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				p = 1;
				mDialog = new ProgressDialog(GalleryActivity.this);
				mDialog.setMessage("Refreshing data...");
	        	mDialog.setCancelable(false);
	        	mDialog.show();
	        	new Thread(new Runnable() {
	        		public void run() {
				        try {
							va = utils.getVideos(String.valueOf(p));
							va.notifyDataSetChanged();
						} 
				        catch (JSONException e) {
							e.printStackTrace();
						}
			            handler.sendEmptyMessage(10);
		    		}
		    	}).start();
			}
		});
        
        ImageButton btn_prev = (ImageButton) findViewById(R.id.btn_left);
        btn_prev.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(p != 1) {
					p = p - 1;
					mDialog = new ProgressDialog(GalleryActivity.this);
					mDialog.setMessage("Fetching previous set of data for \r\nPage " + p);
		        	mDialog.setCancelable(false);
		        	mDialog.show();
		        	new Thread(new Runnable() {
		        		public void run() {
					        try {
								va = utils.getVideos(String.valueOf(p));
								va.notifyDataSetChanged();
							} 
					        catch (JSONException e) {
								e.printStackTrace();
							}
				            handler.sendEmptyMessage(10);
			    		}
			    	}).start();
				}
				else {
					Toast.makeText(getApplicationContext(), "This is the first page", Toast.LENGTH_LONG).show();
				}
			}
		});
    }
    
    private void popProgress() {
    	new Thread(new Runnable() {
			public void run() {
				handler.sendEmptyMessage(1);
			}
		}).start();
    }

    protected class InitTask extends AsyncTask<Context, Integer, String> {
		@Override
		protected String doInBackground(Context... params) {
        	popProgress();
        	new Thread(new Runnable() {
        		public void run() {
    	    		try {
						va = utils.getVideos(String.valueOf(p));
					} 
    	    		catch (JSONException e) {
						e.printStackTrace();
					}
    				p = 1;
					va.notifyDataSetChanged();
			        pc = Integer.valueOf(utils.getPage_cnt());
		            handler.sendEmptyMessage(10);
	    		}
	    	}).start();
			return "COMPLETE!";
		}

        @Override
        protected void onPreExecute() {
            Log.i("makemachine", "onPreExecute()");
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            Log.i( "makemachine", "onProgressUpdate(): " +  String.valueOf( values[0] ) );
        }

        @Override
        protected void onCancelled() {
	        super.onCancelled();
	        Log.i( "makemachine", "onCancelled()" );
        }

        @Override
        protected void onPostExecute( String result ) {
            super.onPostExecute(result);
            Log.i( "makemachine", "onPostExecute(): " + result );
        }
    }    

}
