package com.mci.fof.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

public class FOFActivity extends TabActivity {

    Resources res;
    TabHost tabHost;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fof);
        
        Bundle b = this.getIntent().getExtras();
        final int view_index = b.getInt("view_index");

        res = getResources();
        tabHost = getTabHost();
        TabHost.TabSpec spec;
        Intent intent;

        intent = new Intent().setClass(this, GalleryActivity.class);
        spec = tabHost.newTabSpec("gallery").setIndicator("Gallery", res.getDrawable(R.drawable.gallery)).setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, WhatsnewActivity.class);
        spec = tabHost.newTabSpec("whatsnew").setIndicator("What's new", res.getDrawable(R.drawable.whatsnew)).setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, UploadActivity.class);
        spec = tabHost.newTabSpec("upload").setIndicator("Capture").setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, CelebrationsActivity.class);
        spec = tabHost.newTabSpec("events").setIndicator("Happenings", res.getDrawable(R.drawable.happening_icon)).setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, AboutActivity.class);
        spec = tabHost.newTabSpec("about").setIndicator("About", res.getDrawable(R.drawable.faq)).setContent(intent);
        tabHost.addTab(spec);
        
        tabHost.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.capture_bg);
        tabHost.getTabWidget().getChildAt(2).getLayoutParams().width = 85;
        tabHost.getTabWidget().getChildAt(2).getLayoutParams().height = 117;

        tabHost.getTabWidget().getChildAt(0).getLayoutParams().width = 60;
        tabHost.getTabWidget().getChildAt(0).getLayoutParams().height = 100;

        tabHost.getTabWidget().getChildAt(1).getLayoutParams().width = 60;
        tabHost.getTabWidget().getChildAt(1).getLayoutParams().height = 100;

        tabHost.getTabWidget().getChildAt(3).getLayoutParams().width = 60;
        tabHost.getTabWidget().getChildAt(3).getLayoutParams().height = 100;

        tabHost.getTabWidget().getChildAt(4).getLayoutParams().width = 60;
        tabHost.getTabWidget().getChildAt(4).getLayoutParams().height = 100;

        tabHost.setCurrentTab(view_index);
        
    }
}
