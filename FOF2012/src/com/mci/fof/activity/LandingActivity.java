package com.mci.fof.activity;

import com.mci.fof.util.Constants;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

public class LandingActivity extends Activity {
	
	SharedPreferences prefs;
	SharedPreferences.Editor editor;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.landing);
        
        prefs = getSharedPreferences(Constants.SHARED_PREFS, MODE_PRIVATE);
        Log.i("NRIC", prefs.getString("fullname", "123456789"));
        
        ImageButton btn_watch = (ImageButton) findViewById(R.id.btn_watch);
        ImageButton btn_capture = (ImageButton) findViewById(R.id.btn_capture);
        ImageButton btn_register = (ImageButton) findViewById(R.id.btn_register);
        ImageButton btn_community = (ImageButton) findViewById(R.id.btn_community);
        ImageButton btn_about = (ImageButton) findViewById(R.id.btn_about);
        ImageButton btn_ourcommunity = (ImageButton) findViewById(R.id.btn_ourcommunity);
        
        // watch videos
        btn_watch.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
  	       		Intent mainIntent = new Intent(LandingActivity.this, FOFActivity.class);
	       		mainIntent.putExtra("view_index", 0);
	            LandingActivity.this.startActivity(mainIntent);
			}
		});
        
        // capture a video
        btn_capture.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
	       		Intent mainIntent = new Intent(LandingActivity.this, FOFActivity.class);
	       		mainIntent.putExtra("view_index", 2);
	            LandingActivity.this.startActivity(mainIntent);
			}
		});

        // register
        btn_register.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
	       		Intent mainIntent = new Intent(LandingActivity.this, RegisterActivity.class);
	            LandingActivity.this.startActivity(mainIntent);
			}
		});

        // celebrations
        btn_community.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
	       		Intent mainIntent = new Intent(LandingActivity.this, FOFActivity.class);
	       		mainIntent.putExtra("view_index", 3);
	            LandingActivity.this.startActivity(mainIntent);
			}
		});

        // about
        btn_about.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
	       		Intent mainIntent = new Intent(LandingActivity.this, FOFActivity.class);
	       		mainIntent.putExtra("view_index", 4);
	            LandingActivity.this.startActivity(mainIntent);
			}
		});

        btn_ourcommunity.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
  	       		Intent mainIntent = new Intent(LandingActivity.this, OCWebActivity.class);
	            LandingActivity.this.startActivity(mainIntent);
			}
		});
    }

}
