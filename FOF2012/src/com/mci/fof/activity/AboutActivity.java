package com.mci.fof.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class AboutActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
        
        ImageButton btn_home = (ImageButton) findViewById(R.id.btn_home);
        btn_home.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
	            AboutActivity.this.finish();
			}
		});
        
        ImageButton btn_about_flag = (ImageButton) findViewById(R.id.btn_about_flag);
        btn_about_flag.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
  	       		Intent mainIntent = new Intent(AboutActivity.this, AboutPagesActivity.class);
	       		mainIntent.putExtra("about_index", "flag");
	            AboutActivity.this.startActivity(mainIntent);
			}
		});
        
        ImageButton btn_about_contest = (ImageButton) findViewById(R.id.btn_about_contest);
        btn_about_contest.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
  	       		Intent mainIntent = new Intent(AboutActivity.this, AboutPagesActivity.class);
	       		mainIntent.putExtra("about_index", "contest");
	            AboutActivity.this.startActivity(mainIntent);
			}
		});
        
        ImageButton btn_about_tc = (ImageButton) findViewById(R.id.btn_about_tc);
        btn_about_tc.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
  	       		Intent mainIntent = new Intent(AboutActivity.this, AboutPagesActivity.class);
	       		mainIntent.putExtra("about_index", "tc");
	            AboutActivity.this.startActivity(mainIntent);
			}
		});

        ImageButton btn_about_faq = (ImageButton) findViewById(R.id.btn_about_faq);
        btn_about_faq.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
  	       		Intent mainIntent = new Intent(AboutActivity.this, AboutPagesActivity.class);
	       		mainIntent.putExtra("about_index", "faq");
	            AboutActivity.this.startActivity(mainIntent);
			}
		});

        ImageButton btn_about_pa = (ImageButton) findViewById(R.id.btn_about_pa);
        btn_about_pa.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
  	       		Intent mainIntent = new Intent(AboutActivity.this, AboutPagesActivity.class);
	       		mainIntent.putExtra("about_index", "pa");
	            AboutActivity.this.startActivity(mainIntent);
			}
		});

        ImageButton btn_about_oc = (ImageButton) findViewById(R.id.btn_about_oc);
        btn_about_oc.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
  	       		Intent mainIntent = new Intent(AboutActivity.this, OCWebActivity.class);
	            AboutActivity.this.startActivity(mainIntent);
			}
		});
    }

}
