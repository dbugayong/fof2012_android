package com.mci.fof.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;

import com.mci.fof.util.Constants;

public class SplashActivity extends Activity {
	
	int SPLASH_DISPLAY_LENGTH = 3000;
	ProgressDialog mDialog;
	boolean isValid = true;
	AlertDialog.Builder ab;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        new Handler().postDelayed(new Runnable(){
            public void run() {
            	if(isOnline()) {
            		
            		// ommit this code block after
            		SharedPreferences preferences = getPreferences(MODE_PRIVATE);  
            		SharedPreferences.Editor editor = preferences.edit();
            		editor.putString("user_id", "590");
            		editor.putString("salutation", "Mr.");
    				editor.putString("fullname", "Dennis Bugayong");
            		editor.putString("nric", "A1234567Z");
            		editor.putString("mobile", "+639274739688");
            		editor.putString("email", "dennis@melvindaveconsulting.com");
            		editor.commit();
            		
    	       		Intent mainIntent = new Intent(SplashActivity.this, LandingActivity.class);
    	            SplashActivity.this.startActivity(mainIntent);
    	            SplashActivity.this.finish();
            	}
            	else {
            		ab = new AlertDialog.Builder(SplashActivity.this);
            		ab.setTitle(Constants.APP_TITLE);
            		ab.setMessage("Please make sure that your Android device is connected to the internet.");
            		ab.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							SplashActivity.this.finish();
						}
					});
            		ab.show();
            	}
            }
       }, SPLASH_DISPLAY_LENGTH);
    }
    
    private boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}