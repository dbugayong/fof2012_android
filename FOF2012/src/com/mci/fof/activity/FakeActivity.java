package com.mci.fof.activity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mci.fof.util.Constants;
import com.mci.fof.util.DataUtils;

public class FakeActivity extends Activity {
	
	ProgressDialog mDialog;
    DataUtils utils;
    SharedPreferences prefs;
    SharedPreferences.Editor editor;
    
	// progress dialog handler
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	super.handleMessage(msg);
        	switch (msg.what) {
			case 10:
				mDialog.dismiss();
				Toast.makeText(FakeActivity.this, "Thank you. You are now logged in.", Toast.LENGTH_LONG).show();
				break;
			default:
				break;
			}
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fake);
        
        Bundle b = getIntent().getExtras();
        String vid = b.getString("vid");
        prefs = getSharedPreferences("FOF_USER", MODE_PRIVATE);
        
		utils = new DataUtils(this, DataUtils.getNric());
		JSONObject message = utils.doVote(DataUtils.getNric(), vid);

		try {
			Log.i("FAILED", message.getString("msg"));
		} 
		catch (JSONException e) {
			e.printStackTrace();
		}
		AlertDialog.Builder a = new AlertDialog.Builder(this);
		a.setTitle(Constants.APP_TITLE);
		a.setMessage("You are not logged in. Please login or register before voting.");
		a.setPositiveButton("Login", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface arg0, int arg1) {
				// 
				// create login dialog
				final Dialog login_dialog = new Dialog(FakeActivity.this);
				login_dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
				login_dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, android.R.drawable.ic_dialog_info);
				login_dialog.setContentView(R.layout.login_dialog);
				login_dialog.setTitle(Constants.APP_TITLE);
				
				final EditText nric = (EditText) login_dialog.findViewById(R.id.login_txt_nric);
				final EditText password = (EditText) login_dialog.findViewById(R.id.login_txt_password);
				final AlertDialog.Builder alert = new AlertDialog.Builder(FakeActivity.this);
				alert.setTitle(Constants.APP_TITLE);

				Button cancel = (Button) login_dialog.findViewById(R.id.btn_cancel);
				cancel.setOnClickListener(new View.OnClickListener() {
					public void onClick(View v) {
						login_dialog.dismiss();
						FakeActivity.this.finish();
					}
				});

				Button register = (Button) login_dialog.findViewById(R.id.btn_register);
				register.setOnClickListener(new View.OnClickListener() {
					public void onClick(View arg0) {
						Intent intent = new Intent(FakeActivity.this, RegisterActivity.class);
						FakeActivity.this.startActivity(intent);
						FakeActivity.this.finish();
					}
				});

				Button login = (Button) login_dialog.findViewById(R.id.btn_login);
				login.setOnClickListener(new View.OnClickListener() {
					public void onClick(View arg0) {
						final String _nric = nric.getText().toString();
						final String _password = password.getText().toString();
						
						Log.i("Login", "Login data: " + _nric + " - " + _password);
						
						if(!validateLogin(_nric, _password)) {
							alert.setMessage("Please fill in NRIC and Password fields!");
							alert.setNeutralButton("Okay", new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface arg0, int arg1) {
									// do nothing
								}
							});
							alert.show();
						}
						else {
							// request for login
							mDialog = new ProgressDialog(FakeActivity.this);
							mDialog.setMessage("Loggin you in, please wait...");
					    	mDialog.setCancelable(false);
					    	mDialog.show();
					    	new Thread(new Runnable() {
					    		public void run() {
									JSONObject message = utils.doLogin(nric.getText().toString(), password.getText().toString());
									try {
										int result = message.getInt("result");
										String msg = message.getString("msg");
										if(result == 1) {
											Log.i("Login result", msg);

											JSONObject u = message.getJSONObject("user_info");
											editor = prefs.edit();
											editor.putInt("user_id", u.getInt("user_id"));
											editor.putString("salutation", u.getString("salutation"));
											editor.putString("fullname", u.getString("fullname"));
											editor.putString("nric", u.getString("nric"));
											editor.putString("mobile", u.getString("mobile"));
											editor.putString("email", u.getString("email"));
											editor.commit();

											login_dialog.dismiss();
//											Toast.makeText(FakeActivity.this, "Thank you. You are now logged in.", Toast.LENGTH_LONG).show();
											handler.sendEmptyMessage(10);
											Intent intent = new Intent(FakeActivity.this, FOFActivity.class);
											intent.putExtra("view_index", 0);
											FakeActivity.this.startActivity(intent);
											FakeActivity.this.finish();
										}
										else {
											Log.i("Login result", msg);
										}
									} 
									catch (JSONException e) {
										e.printStackTrace();
									}
					    		}
					    	}).start();
						}
					}
				});
				login_dialog.show();
			}
		});
		a.setNeutralButton("Register", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				//
				Intent intent = new Intent(FakeActivity.this, RegisterActivity.class);
				FakeActivity.this.startActivity(intent);
				FakeActivity.this.finish();
			}
		});
		a.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				FakeActivity.this.finish();
			}
		});
		a.show();
    }
    
    private final boolean validateLogin(String nric, String passwd) {
    	boolean valid = true;
    	if(nric.equals(null) || nric.equals("")) {
    		valid = false;
    	}
    	if(passwd.equals(null) || passwd.equals("")) {
    		valid = false;
    	}
    	return valid;
    }


}
