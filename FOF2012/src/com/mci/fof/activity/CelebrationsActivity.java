package com.mci.fof.activity;

import org.json.JSONException;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.mci.fof.adapter.EventAdapter;
import com.mci.fof.tv.EventTextView;
import com.mci.fof.util.DataUtils;

public class CelebrationsActivity extends ListActivity {
	
	private static int p = 1;
	private static int pc = 0;
	DataUtils utils;
	EventAdapter ea;

	private ProgressDialog mDialog;
	protected InitTask initTask;
	
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	super.handleMessage(msg);
        	switch (msg.what) {
			case 10:
				setListAdapter(ea);
				mDialog.dismiss();
				break;
			case 1:
				mDialog.setMessage("Fetching events...");
	        	mDialog.setCancelable(false);
				mDialog.show();
				break;
			default:
				break;
			}
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.white);

        setContentView(R.layout.celebrations);
        ImageButton btn_home = (ImageButton) findViewById(R.id.btn_home);
        btn_home.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
	            CelebrationsActivity.this.finish();
			}
		});
        
		mDialog = new ProgressDialog(CelebrationsActivity.this);
		utils = new DataUtils(CelebrationsActivity.this);
        initTask = new InitTask();

        initTask = new InitTask();
        initTask.execute(CelebrationsActivity.this);
        setListAdapter(ea);
        
        ImageButton btn_next = (ImageButton) findViewById(R.id.btn_right);
        btn_next.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(p == pc) {
					Toast.makeText(getApplicationContext(), "This is the last page", Toast.LENGTH_LONG).show();
				}
				else {
					p = p + 1;
					mDialog = new ProgressDialog(CelebrationsActivity.this);
					mDialog.setMessage("Fetching next set of data for \r\nPage " + p);
		        	mDialog.setCancelable(false);
		        	mDialog.show();
		        	new Thread(new Runnable() {
		        		public void run() {
					        try {
								ea = utils.getEvents(String.valueOf(p));
								ea.notifyDataSetChanged();
							} 
					        catch (JSONException e) {
								e.printStackTrace();
							}
				            handler.sendEmptyMessage(10);
			    		}
			    	}).start();
				}
			}
		});
        
        ImageButton btn_refresh = (ImageButton) findViewById(R.id.btn_refresh);
        btn_refresh.setOnClickListener(new View.OnClickListener() {
			public void onClick(View arg0) {
				p = 1;
				mDialog = new ProgressDialog(CelebrationsActivity.this);
				mDialog.setMessage("Refreshing data...");
	        	mDialog.setCancelable(false);
	        	mDialog.show();
	        	new Thread(new Runnable() {
	        		public void run() {
				        try {
							ea = utils.getEvents(String.valueOf(p));
							ea.notifyDataSetChanged();
						} 
				        catch (JSONException e) {
							e.printStackTrace();
						}
			            handler.sendEmptyMessage(10);
		    		}
		    	}).start();
			}
		});
        
        ImageButton btn_prev = (ImageButton) findViewById(R.id.btn_left);
        btn_prev.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(p != 1) {
					p = p - 1;
					mDialog = new ProgressDialog(CelebrationsActivity.this);
					mDialog.setMessage("Fetching previous set of data for \r\nPage " + p);
		        	mDialog.setCancelable(false);
		        	mDialog.show();
		        	new Thread(new Runnable() {
		        		public void run() {
					        try {
								ea = utils.getEvents(String.valueOf(p));
								ea.notifyDataSetChanged();
							} 
					        catch (JSONException e) {
								e.printStackTrace();
							}
				            handler.sendEmptyMessage(10);
			    		}
			    	}).start();
				}
				else {
					Toast.makeText(getApplicationContext(), "This is the first page", Toast.LENGTH_LONG).show();
				}
			}
		});
        
        ListView lv = getListView();
        lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				EventTextView tv = (EventTextView) arg1;
				String event_id = tv.getEvent_id().getText().toString();
				String name = tv.getName().getText().toString();
				String event_date = tv.getEvent_date().getText().toString();
				String end_date = tv.getEnd_date().getText().toString();
				String start_time = tv.getStart_time().getText().toString();
				String end_time = tv.getEnd_time().getText().toString();
				String venue = tv.getVenue().getText().toString();
				String event_desc = tv.getEvent_desc().getText().toString();
				String thumb = tv.getImage_url().getText().toString();
				
				String when = "";
				if(end_date == "" || end_date == null) {
					when = event_date;
				}
				else {
					when = event_date + " to " + end_date;
				}
				
				String time = "";
				if(end_time == null || end_time == "") {
					time = start_time + " to " + end_time;
				}
				
//				Toast.makeText(CelebrationsActivity.this, "" + event_id + "-" + name + "-" + event_date + "-" + end_date + "-" + start_time + "-" + end_time + "-" + venue + "-" + event_desc + "-" + thumb, Toast.LENGTH_LONG).show();
				Log.i("EVENT INFO", event_id + "-" + name + "-" + event_date + "-" + end_date + "-" + start_time + "-" + end_time + "-" + venue + "-" + event_desc + "-" + thumb);
				Intent intent = new Intent(CelebrationsActivity.this, EventViewActivity.class);
				intent.putExtra("event_id", event_id);
				intent.putExtra("name", name);
				intent.putExtra("thumb", thumb);
				intent.putExtra("where", venue);
				intent.putExtra("when", when);
				intent.putExtra("time", time);
				intent.putExtra("desc", event_desc);
				CelebrationsActivity.this.startActivity(intent);
			}
        });

    }

    private void popProgress() {
    	new Thread(new Runnable() {
			public void run() {
				handler.sendEmptyMessage(1);
			}
		}).start();
    }

    protected class InitTask extends AsyncTask<Context, Integer, String> {
		@Override
		protected String doInBackground(Context... params) {
        	popProgress();
        	new Thread(new Runnable() {
        		public void run() {
    	    		try {
						ea = utils.getEvents(String.valueOf(p));
					} 
    	    		catch (JSONException e) {
						e.printStackTrace();
					}
    				p = 1;
					ea.notifyDataSetChanged();
			        pc = Integer.valueOf(utils.getPage_cnt());
		            handler.sendEmptyMessage(10);
	    		}
	    	}).start();
			return "COMPLETE!";
		}

        @Override
        protected void onPreExecute() {
            Log.i("makemachine", "onPreExecute()");
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            Log.i( "makemachine", "onProgressUpdate(): " +  String.valueOf( values[0] ) );
        }

        @Override
        protected void onCancelled() {
	        super.onCancelled();
	        Log.i( "makemachine", "onCancelled()" );
        }

        @Override
        protected void onPostExecute( String result ) {
            super.onPostExecute(result);
            Log.i( "makemachine", "onPostExecute(): " + result );
        }
    }    

}
