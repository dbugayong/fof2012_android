package com.mci.fof.activity;

import java.io.File;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mci.fof.util.Constants;
import com.mci.fof.util.DataUtils;

public class UploadActivity extends Activity {
	
	private boolean isValid = true;
	EditText title;
	EditText desc;
	AlertDialog.Builder ab;
	SharedPreferences prefs;
	SharedPreferences.Editor editor;
	String def = "Default"; 
	final String lib_items[] = { "Yes, login as another user", "No, proceed to library and upload", "Cancel" };
	final String rec_items[] = { "Yes, login as another user", "No, proceed to record and upload", "Cancel" };
    EditText nric;
    EditText password;
    DataUtils utils;
	ProgressDialog mDialog;
	Dialog dialog = null;
	Dialog login_dialog = null;
	private final static int ACTIVITY_SELECT_IMAGE = 505;
	String action = "def";
	
	String vid_title = null;
	String vid_desc = null;
	File file;
	String res = null;
	String mes = null;
	protected InitTask initTask;
	
	// progress dialog handler
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        	super.handleMessage(msg);
        	switch (msg.what) {
			case 10:
				mDialog.dismiss();
				if(action.equals("upload")) {
			    	AlertDialog.Builder aa = new AlertDialog.Builder(UploadActivity.this);
			    	aa.setTitle(Constants.APP_TITLE);
			    	aa.setMessage(mes);
			    	aa.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
							// nothing to do here
						}
					});
			    	aa.show();
				}
				break;
			default:
				break;
			}
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload);
        
        title = (EditText) findViewById(R.id.txt_title);
        desc = (EditText) findViewById(R.id.txt_desc);
        prefs = getSharedPreferences("FOF_USER", MODE_PRIVATE);
        
        utils = new DataUtils(UploadActivity.this);
        ab = new AlertDialog.Builder(UploadActivity.this);
        mDialog = new ProgressDialog(UploadActivity.this);
        
		Log.i("USER_INFO", String.valueOf(prefs.getInt("user_id", 0)));
        Log.i("USER_INFO", prefs.getString("fullname", "User Fullname"));
        Log.i("USER_INFO", prefs.getString("nric", "User NRIC"));
        
        ImageButton btn_home = (ImageButton) findViewById(R.id.btn_home);
        btn_home.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
	            UploadActivity.this.finish();
			}
		});

        Button btn_library = (Button) findViewById(R.id.btn_library);
        btn_library.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				String msg = validateThis(title.getText().toString(), desc.getText().toString());
				if(!isValid) {
					msg = "Please fill the following fields:\r\n\r\n" + msg;
					ab.setTitle(Constants.APP_TITLE);
					ab.setMessage(msg);
					ab.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
							isValid = true;
						}
					});
					ab.show();
				}
				else {
					// ask user if login as another user or proceed
					dialog = new Dialog(UploadActivity.this);
					dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
					dialog.setContentView(R.layout.upload_dialog);
					dialog.setTitle(Constants.APP_TITLE);
					dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, android.R.drawable.ic_dialog_info);

					Button noButton = (Button) dialog.findViewById(R.id.btn_no);
					Button yesButton = (Button) dialog.findViewById(R.id.btn_yes);
					Button cancelButton = (Button) dialog.findViewById(R.id.btn_cancel);

					TextView lbl = (TextView) dialog.findViewById(R.id.lbl_msg);
					if(prefs.getString("fullname", "Full Name").equals("Full Name")) {
						lbl.setText("You are not logged in. Please login or register.");
						noButton.setEnabled(false);
					}
					else {
						lbl.setText("You are logged in as " + prefs.getString("fullname", "Full Name") + ", do you want to upload a video using another account?");
					}
					// user want to login using another account
					yesButton.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							// create login dialog
							login_dialog = new Dialog(UploadActivity.this);
							login_dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
							login_dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, android.R.drawable.ic_dialog_info);
							login_dialog.setContentView(R.layout.login_dialog);
							login_dialog.setTitle(Constants.APP_TITLE);
							
							nric = (EditText) login_dialog.findViewById(R.id.login_txt_nric);
							password = (EditText) login_dialog.findViewById(R.id.login_txt_password);
							final AlertDialog.Builder alert = new AlertDialog.Builder(UploadActivity.this);
							alert.setTitle(Constants.APP_TITLE);

							Button login = (Button) login_dialog.findViewById(R.id.btn_login);
							login.setOnClickListener(new View.OnClickListener() {
								public void onClick(View arg0) {
									final String _nric = nric.getText().toString();
									final String _password = password.getText().toString();
									
									Log.i("Login", "Login data: " + _nric + " - " + _password);
									
									if(!validateLogin(_nric, _password)) {
										alert.setMessage("Please fill in NRIC and Password fields!");
										alert.setNeutralButton("Okay", new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface arg0, int arg1) {
												// do nothing
											}
										});
										alert.show();
									}
									else {
										// request for login
										mDialog = new ProgressDialog(UploadActivity.this);
										mDialog.setMessage("Loggin you in, please wait...");
								    	mDialog.setCancelable(false);
								    	mDialog.show();
								    	new Thread(new Runnable() {
								    		public void run() {
												JSONObject message = utils.doLogin(nric.getText().toString(), password.getText().toString());
												try {
													int result = message.getInt("result");
													String msg = message.getString("msg");
													if(result == 1) {
														Log.i("Login result", msg);

														JSONObject u = message.getJSONObject("user_info");
														editor = prefs.edit();
														editor.putInt("user_id", u.getInt("user_id"));
														editor.putString("salutation", u.getString("salutation"));
														editor.putString("fullname", u.getString("fullname"));
														editor.putString("nric", u.getString("nric"));
														editor.putString("mobile", u.getString("mobile"));
														editor.putString("email", u.getString("email"));
														editor.commit();

														login_dialog.dismiss();
														// send alert and dismiss dialog
														postLogin(msg);
													}
													else {
														Log.i("Login result", msg);
													}
												} 
												catch (JSONException e) {
													e.printStackTrace();
												}
									            handler.sendEmptyMessage(10);
								    		}
								    	}).start();
									}
								}
							});
							
							Button register = (Button) login_dialog.findViewById(R.id.btn_register);
							register.setOnClickListener(new View.OnClickListener() {
								public void onClick(View arg0) {
									Intent intent = new Intent(UploadActivity.this, RegisterActivity.class);
									UploadActivity.this.startActivity(intent);
									UploadActivity.this.finish();
								}
							});
							
							Button cancel = (Button) login_dialog.findViewById(R.id.btn_cancel);
							cancel.setOnClickListener(new View.OnClickListener() {
								public void onClick(View v) {
									login_dialog.dismiss();
								}
							});
							login_dialog.show();
						}
						
					});

					// user wants to proceed with upload
					noButton.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							dialog.dismiss();
							vid_title = title.getText().toString();
							vid_desc = desc.getText().toString();
							Intent videoPicker = new Intent(Intent.ACTION_GET_CONTENT);
							videoPicker.setType("video/*");
							startActivityForResult(videoPicker, ACTIVITY_SELECT_IMAGE);
						}
					});

					// user cancels
					cancelButton.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							dialog.dismiss();
						}
					});
					dialog.show();
				}
			}
		});
        
        Button btn_record = (Button) findViewById(R.id.btn_record);
        btn_record.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				String msg = validateThis(title.getText().toString(), desc.getText().toString());
				if(!isValid) {
					msg = "Please fill the following fields:\r\n\r\n" + msg;
					ab.setTitle(Constants.APP_TITLE);
					ab.setMessage(msg);
					ab.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
							isValid = true;
						}
					});
					ab.show();
				}
				else {
					// ask user if login as another user or proceed
					dialog = new Dialog(UploadActivity.this);
					dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
					dialog.setContentView(R.layout.upload_dialog);
					dialog.setTitle(Constants.APP_TITLE);
					dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, android.R.drawable.ic_dialog_info);

					Button noButton = (Button) dialog.findViewById(R.id.btn_no);
					Button yesButton = (Button) dialog.findViewById(R.id.btn_yes);
					Button cancelButton = (Button) dialog.findViewById(R.id.btn_cancel);

					TextView lbl = (TextView) dialog.findViewById(R.id.lbl_msg);
					if(prefs.getString("fullname", "Full Name").equals("Full Name")) {
						lbl.setText("You are not logged in. Please login or register.");
						noButton.setEnabled(false);
					}
					else {
						lbl.setText("You are logged in as " + prefs.getString("fullname", "Full Name") + ", do you want to upload a video using another account?");
					}
					// user want to login using another account
					yesButton.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							// create login dialog
							login_dialog = new Dialog(UploadActivity.this);
							login_dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
							login_dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, android.R.drawable.ic_dialog_info);
							login_dialog.setContentView(R.layout.login_dialog);
							login_dialog.setTitle(Constants.APP_TITLE);
							
							nric = (EditText) login_dialog.findViewById(R.id.login_txt_nric);
							password = (EditText) login_dialog.findViewById(R.id.login_txt_password);
							final AlertDialog.Builder alert = new AlertDialog.Builder(UploadActivity.this);
							alert.setTitle(Constants.APP_TITLE);

							Button login = (Button) login_dialog.findViewById(R.id.btn_login);
							login.setOnClickListener(new View.OnClickListener() {
								public void onClick(View arg0) {
									final String _nric = nric.getText().toString();
									final String _password = password.getText().toString();
									
									Log.i("Login", "Login data: " + _nric + " - " + _password);
									
									if(!validateLogin(_nric, _password)) {
										alert.setMessage("Please fill in NRIC and Password fields!");
										alert.setNeutralButton("Okay", new DialogInterface.OnClickListener() {
											public void onClick(DialogInterface arg0, int arg1) {
												// do nothing
											}
										});
										alert.show();
									}
									else {
										// request for login
										mDialog = new ProgressDialog(UploadActivity.this);
										mDialog.setMessage("Loggin you in, please wait...");
								    	mDialog.setCancelable(false);
								    	mDialog.show();
								    	new Thread(new Runnable() {
								    		public void run() {
												JSONObject message = utils.doLogin(nric.getText().toString(), password.getText().toString());
												try {
													int result = message.getInt("result");
													String msg = message.getString("msg");
													if(result == 1) {
														Log.i("Login result", msg);

														JSONObject u = message.getJSONObject("user_info");
														editor = prefs.edit();
														editor.putInt("user_id", u.getInt("user_id"));
														editor.putString("salutation", u.getString("salutation"));
														editor.putString("fullname", u.getString("fullname"));
														editor.putString("nric", u.getString("nric"));
														editor.putString("mobile", u.getString("mobile"));
														editor.putString("email", u.getString("email"));
														editor.commit();

														login_dialog.dismiss();
														// send alert and dismiss dialog
														postLogin(msg);
													}
													else {
														Log.i("Login result", msg);
													}
												} 
												catch (JSONException e) {
													e.printStackTrace();
												}
									            handler.sendEmptyMessage(10);
								    		}
								    	}).start();
									}
								}
							});
							
							Button register = (Button) login_dialog.findViewById(R.id.btn_register);
							register.setOnClickListener(new View.OnClickListener() {
								public void onClick(View arg0) {
									Intent intent = new Intent(UploadActivity.this, RegisterActivity.class);
									UploadActivity.this.startActivity(intent);
									UploadActivity.this.finish();
								}
							});
							
							Button cancel = (Button) login_dialog.findViewById(R.id.btn_cancel);
							cancel.setOnClickListener(new View.OnClickListener() {
								public void onClick(View v) {
									login_dialog.dismiss();
								}
							});
							login_dialog.show();
						}
						
					});

					// user wants to proceed with upload
					noButton.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							dialog.dismiss();
							vid_title = title.getText().toString();
							vid_desc = desc.getText().toString();
							Intent i = new Intent("android.media.action.VIDEO_CAPTURE");
							startActivityForResult(i, ACTIVITY_SELECT_IMAGE);
						}
					});

					// user cancels
					cancelButton.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							dialog.dismiss();
						}
					});
					dialog.show();
				}
			}
		});
        
        Button btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				UploadActivity.this.finish();
			}
		});
    }
    
    private String validateThis(String title, String desc) {
    	String msg = "";
    	if(title.equals(null) || title.equals("")) {
    		msg = msg + "\t- Title\r\n";
    		isValid = false;
    	}
    	if(desc.equals(null) || desc.equals("")) {
    		msg = msg + "\t- Description";
    		isValid = false;
    	}
    	return msg;
    }
    
    private final boolean validateLogin(String nric, String passwd) {
    	boolean valid = true;
    	if(nric.equals(null) || nric.equals("")) {
    		valid = false;
    	}
    	if(passwd.equals(null) || passwd.equals("")) {
    		valid = false;
    	}
    	return valid;
    }
    
    private final void postLogin(String msg) {
    	dialog.dismiss();
    	action = "login";
    	final String m = msg;
    	Activity activity = getParent();
    	activity.runOnUiThread(new Runnable() {
			public void run() {
				AlertDialog.Builder a = new AlertDialog.Builder(UploadActivity.this);
				a.setTitle(Constants.APP_TITLE);
				a.setMessage(m);
				a.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface arg0, int arg1) {
					}
				});
				a.show();
			}
		});
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
        Log.i("File path", "onActivityResult " + ACTIVITY_SELECT_IMAGE);
        switch(requestCode) { 
        case ACTIVITY_SELECT_IMAGE:
        	Log.i("File path", "resultCode " + resultCode + " OR " + RESULT_OK);
            if(resultCode == RESULT_OK) {
            	// do task here
				mDialog.setMessage("Uploading your video...");
	        	mDialog.setCancelable(false);
				mDialog.show();
                Uri selectedImage = imageReturnedIntent.getData();
                Log.i("File path", getRealPathFromURI(selectedImage));
                file = new File(getRealPathFromURI(selectedImage));
                initTask = new InitTask();
                initTask.execute(UploadActivity.this);
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    
    protected class InitTask extends AsyncTask<Context, Integer, String> {
		@Override
		protected String doInBackground(Context... params) {
        	new Thread(new Runnable() {
        		public void run() {
        			Log.i("initTask", "Task running");
        			String aUrl = Constants.UPLOAD_URL; 
        	        HttpClient httpClient = new DefaultHttpClient();
        	        HttpPost httpPost = new HttpPost(aUrl);
        	        JSONObject message = null;
        	        try {
        	            MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        	            entity.addPart("user_file", new FileBody(file));
        	            entity.addPart("title", new StringBody(vid_title));
        	            entity.addPart("description", new StringBody(vid_desc));
        	            entity.addPart("nric", new StringBody(prefs.getString("nric", "000000000")));
        	            httpPost.setEntity(entity);

        	            HttpContext localContext = new BasicHttpContext();
        	            HttpResponse response = httpClient.execute(httpPost, localContext);
        	            HttpEntity resEntity = response.getEntity();  
        	            String resp = "";
        	            if (response != null) {    
        	                resp = EntityUtils.toString(resEntity); 
        	            }
        				message = new JSONObject(resp);
        	            Log.i("doUploadVideo RESULT", message.getString("result"));
        	            res = message.getString("result");
        	            mes = message.getString("msg");
        	            action = "upload";
        	        } 
        	        catch (IOException e) {
        	            e.printStackTrace();
        	        } 
        	        catch (JSONException e) {
        				e.printStackTrace();
        			}
        	        handler.sendEmptyMessage(10);
	    		}
	    	}).start();
			return "COMPLETE!";
		}

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i("makemachine", "onPreExecute()");
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            Log.i( "makemachine", "onProgressUpdate(): " +  String.valueOf( values[0] ) );
        }

        @Override
        protected void onCancelled() {
	        super.onCancelled();
	        Log.i( "makemachine", "onCancelled()" );
        }

        @Override
        protected void onPostExecute( String result ) {
            super.onPostExecute(result);
            Log.i( "makemachine", "onPostExecute(): " + result );
        }
    }    


}
