package com.mci.fof.activity;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class OCWebActivity extends Activity {

	WebView mWebView;

	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.oc_web);
	    
	    try {
		    mWebView = (WebView) findViewById(R.id.webview);
		    mWebView.setWebViewClient(new NewsWebViewClient());
		    mWebView.getSettings().setJavaScriptEnabled(true);
		    mWebView.loadUrl("http://www.ourcommunity.sg/");
	    }
	    catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private class NewsWebViewClient extends WebViewClient {
	    @Override
	    public boolean shouldOverrideUrlLoading(WebView view, String url) {
	    	try {
	    		view.loadUrl(url);
	    	}
	    	catch (Exception e) {
				e.printStackTrace();
			}
	        return true;
	    }
	}

}
